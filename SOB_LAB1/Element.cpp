#include "stdafx.h"
#include "Element.h"

Element::Element(){
	this->value = 0;
	this->prev = nullptr;
	this->next = nullptr;
}
Element::Element(int val){
	this->value = val;
	this->prev = nullptr;
	this->next = nullptr;
}
Element::~Element(){
}
int Element::get_value(){
	return value;
}
void Element::set_value(int val){
	this->value = val;
}
Element * Element::get_next(){
	return next;
}
void Element::set_next(Element * next){
	this->next = next;
}
Element * Element::get_prev(){
	return prev;
}
void Element::set_prev(Element * prev){
	this->prev = prev;
}
