#pragma once
#include "stdafx.h"
#include <iostream>
#include "List.h"
#include <string>
#include <sstream>
using std::string;

class Util {

public:
	static List * loadListFromFile(string fileName, int minVal, int maxVal);
	template<typename T>
	static string valueToString(T value);
	template<typename T>
	static T stringToValue(string str);
	static int stringToInt(string str);
};

template<typename T>
static string Util::valueToString(T value)
{
	ostringstream oss;
	oss << value;
	return oss.str();
}

template<typename T>
static T Util::stringToValue(string str)
{
	istringstream iss(str);
	T value;
	iss >> value;
	return value;
}