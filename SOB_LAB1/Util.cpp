#include "stdafx.h"
#include "Util.h"
#include <iostream>
#include <fstream>
#include <string>
#define NDEBUG
#include <cassert>
using namespace std;

int ignoreWhiteMarks(istream& in){
	assert(in);
	int howMany = 0;
	while (in.peek() == 10 || in.peek() == 32){
		in.ignore();
		howMany++;
	}
	return howMany;
}
int ignoreSemicolons(istream& in){
	assert(in);
	int howMany = 0;
	while (in.peek() == 10 || in.peek() == 59) {
		in.ignore();
		howMany++;
	}
	return howMany;
}

inline bool ifFileExists(const std::string& name){
	ifstream f(name.c_str());
	return f.good();
}
bool isEmpty(std::istream& pFile)
{
	return pFile.peek() == std::ifstream::traits_type::eof();
}

List * Util::loadListFromFile(string fileName, int minVal, int maxVal){
	assert(ifFileExists(fileName));
	List * list = new List();
	assert(list != nullptr);

	if (ifFileExists(fileName) && list != nullptr) {
		int value;
		char what;
		string str;
		bool sign = true;

		if (minVal > maxVal) {
			int tmp = minVal;
			minVal = maxVal;
			maxVal = tmp;
		}
		// open file
		filebuf* fb = new filebuf();
		fb->open(fileName, ios::in);

		assert(fb->is_open());
		if (fb->is_open()) {
			istream plik(fb);

			// load list from file
			assert(!isEmpty(plik));
			if (!isEmpty(plik)) {
				while (!plik.eof()) {
					sign = true;
					ignoreWhiteMarks(plik);
					what = plik.peek();
					if (what == 45) {
						sign = false;
						plik.ignore();
						what = plik.peek();
					}
					if (isdigit(what)) {
						plik >> value;
						if (!sign)
							value = -value;
						if (value >= minVal && value <= maxVal)
							list->add(value);
						else {
							assert(value >= minVal && value <= maxVal);
							cout << "Incorrect value in file: " << value << endl;
						}
					}
					else {
						plik >> str;
						assert("NaN in file" == "");
						cout << "NaN in file: " << str << endl;
					}
				}
			}
			else {
				cout << "File " << fileName << " is empty!" << endl;
			}
			fb->close();
		}
	}
	return list;
}

int Util::stringToInt(string str)
{
	string::iterator it;
	bool isNumber = true;
	bool isPositive = true;
	for (it = str.begin(); it < str.end(); it++)
	{
		char ch = *it;
		if (ch == '-' && it == str.begin()) {
			isPositive = false;
			cout << "Not positive" << endl;
		}
		else if (!isdigit(ch)) {
		isNumber = false;
		cout << "NAN" << endl;
		}
	}
	if (isNumber) {
		if (isPositive) {
			cout << "Convert positive" << endl;
			istringstream iss(str);
			int value;
			iss >> value;
			return value;
		}
		else {
			cout << "Convert negative" << endl;
			istringstream iss(str.substr(1,str.size()));
			int value;
			iss >> value;
			value = -value;
			return value;
		}
	}
	return NULL;
}
