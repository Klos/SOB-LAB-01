#include "stdafx.h"
#include "List.h"
//#define NDEBUG
#include <cassert>
Element * List::addHead(int a){
	Element *n = new Element(a);
	assert(n != nullptr);

	if (head != nullptr) {
		head->set_prev(n);
		head = n;
	}
	else {
		head = n;
		tail = n;
	}
	return n;
}

Element * List::addTail(int a){
	Element *n = new Element(a);
	assert(n != nullptr);

	if (tail != nullptr) {
		tail->set_next(n);
		tail = n;
	}
	else {
		head = n;
		tail = n;
	}
	return n;
}

void List::removeHead(){
	assert(head != nullptr);
	if (head != nullptr) {
		if (head == tail) {
			delete head;
			head = nullptr;
			tail = nullptr;
		}
		else {
			head = head->get_next();
			delete head->get_prev();
			head->set_prev(nullptr);
		}
	}
}

void List::removeTail(){
	assert(tail != nullptr);
	if (tail != nullptr) {
		if (tail == head) {
			delete head;
			head = nullptr;
			tail = nullptr;
		}
		else {
			tail = tail->get_prev();
			delete tail->get_next();
			tail->set_next(nullptr);
		}
	}
}

List::List(){
	this->head = nullptr;
	this->tail = nullptr;
}

List::~List(){
	removeList();
}

Element * List::add(int a){
	Element *n = new Element(a);
	assert(n != nullptr);

	if (head != nullptr) {
		Element *p = head;
		while (p->get_next() != nullptr && p->get_next()->get_value() < n->get_value()) {
			p = p->get_next();
		}
		// insert before p
		if (p->get_value() > n->get_value()) {
			// new head
			if (p == head) {
				head = n;
				p->set_prev(n);
				n->set_next(p);
				n->set_prev(nullptr);
			}
			// insert middle
			else {
				n->set_prev(p->get_prev());
				n->get_prev()->set_next(n);
				p->set_prev(n);
				n->set_next(p);
			}
		}
		// insert aftrer p
		else {
			// new tail
			if (p == tail) {
				tail = n;
				n->set_next(nullptr);
				n->set_prev(p);
				p->set_next(n);
			}
			// insert middle
			else {
				n->set_prev(p);
				p->get_next()->set_prev(n);
				n->set_next(p->get_next());
				p->set_next(n);
			}
		}
	}
	else {	// empty list
		head = n;
		tail = n;
	}
	return n;
}

void List::removeValue(int a){
	Element *n = head;
	while (n != nullptr && n->get_value() != a)
		n = n->get_next();
	if (n != nullptr) {
		if (n == head) {	// head
			if (head->get_next() != nullptr) {
				head->get_next()->set_prev(nullptr);
				head = head->get_next();
			}
			else {// head + tail
				head = nullptr;
				tail = nullptr;
			}
			delete n;
		}
		else if (n == tail) {	// tail
			n->get_prev()->set_next(nullptr);
			tail = n->get_prev();
			delete n;
		}
		else {	// middle element
			n->get_prev()->set_next(n->get_next());
			n->get_next()->set_prev(n->get_prev());
			delete n;
		}
	}
}

void List::removeList(){
	if (head != nullptr) {
		Element *tmp = head;
		while (tmp->get_next() != nullptr) {
			tmp = tmp->get_next();
			delete tmp->get_prev();
		}
		delete tmp;
		head = nullptr;
		tail = nullptr;
	}
}

void List::showRightToLeft(){
	if (tail != nullptr) {
		Element *tmp = tail;
		do {
			std::cout << tmp->get_value() << " | ";
			tmp = tmp->get_prev();
		} while (tmp != nullptr);
		std::cout << std::endl;
	}
}

void List::showLeftToRight(){
	if (head != nullptr) {
		Element *tmp = head;
		do {
			std::cout << tmp->get_value() << " | ";
			tmp = tmp->get_next();
		} while (tmp != nullptr);
		std::cout << std::endl;
	}
}

void List::swap(Element *& p, Element *& q){
	assert(p != nullptr);
	assert(q != nullptr);
	// TODO swap pointers or swap values?
	if (p != nullptr && q != nullptr) {
		Element *tmp = p;
		p = q;
		q = tmp;
	}
}
