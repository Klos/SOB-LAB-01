#pragma once
#include "stdafx.h"
#include <iostream>
#include "Element.h"

class List {
private:
	Element *head;
	Element *tail;
	Element *addHead(int a);
	Element *addTail(int a);
	void removeHead();
	void removeTail();
public:
	List();
	virtual ~List();
	Element *add(int a); // ascending order
	void removeValue(int a);
	void removeList();
	void showRightToLeft();
	void showLeftToRight();
	void swap(Element *&p, Element *&q);
};