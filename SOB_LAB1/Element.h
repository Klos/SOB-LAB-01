#pragma once
#include "stdafx.h"
#include <iostream>

class Element {
private:
	int value;
	Element *next;
	Element *prev;
public:
	Element();
	Element(int val);
	virtual ~Element();

	int get_value();
	void set_value(int val);

	Element *get_next();
	void set_next(Element *next);

	Element *get_prev();
	void set_prev(Element *prev);
};
