// SOBLAB1.cpp : Defines the entry point for the console application.
//
#include "stdafx.h"
#include <string>
#include <iostream>
#include "Util.h"
#include "List.h"
#define NDEBUG
#include <cassert>
using namespace std;

int main(int argc, char *argv[]){

	cout << "Args:" << endl;
	for (int i = 1; i < argc; i++)
		cout << "arg" << i << ": " << argv[i] << endl;

	assert(argc >= 3);
	if (argc >= 3) {
		string filename = argv[1];
		int minVal = INT_MIN, maxVal = INT_MAX;
		if (argc >= 4) {
			minVal = Util::stringToValue<int>(string(argv[2]));
			maxVal = Util::stringToValue<int>(string(argv[3]));	
		}
		else 
			maxVal = Util::stringToValue<int>(string(argv[2]));

		List list = *Util::loadListFromFile(filename, minVal, maxVal);
		// do something there
		cout << "Left to Right:" << endl;
		list.showLeftToRight();
		cout << "Add some values" << endl;
		list.add(3);
		list.add(2);
		list.add(12);
		list.add(-100); 
		list.add(-100);
		list.add(-15);
		list.add(-50);
		cout << "Left to Right:" << endl;
		list.showLeftToRight();
		cout << "Remove value -100" << endl;
		list.removeValue(-100);
		cout << "Left to Right:" << endl;
		list.showLeftToRight();
		cout << "Right to Left:" << endl;
		list.showRightToLeft();
	}
	else {
		cout << "Too few arguments" << endl << "1st -> filename" << endl
			<< "2nd -> List minVal" << endl << "3rd -> list maxVal" << endl;
	}
	getchar();
    return 0;
}